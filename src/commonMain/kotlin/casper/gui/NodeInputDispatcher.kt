package casper.gui

import casper.input.InputDispatcher
import casper.input.MouseEvent
import casper.signal.concrete.Future

class MouseOut(val node: UINode) : MouseEvent()
class MouseOver(val node: UINode) : MouseEvent()

interface NodeInputDispatcher : InputDispatcher {
	val onMouseOut: Future<MouseOut>
	val onMouseOver: Future<MouseOver>
}
