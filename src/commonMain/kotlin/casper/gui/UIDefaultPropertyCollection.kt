package casper.gui

import casper.signal.concrete.Signal
import kotlin.reflect.KClass

class UIDefaultPropertyCollection : UIPropertyCollection {
	val values = mutableMapOf<Any, UIProperty>()
	override val propertySignal  = Signal<UIProperty>()

	override fun <T : UIProperty> set(value: T) {
		values.put(value::class, value)
		propertySignal.set(value)
	}

	override fun <T : UIProperty> get(clazz: KClass<T>): T? {
		val item = values.get(clazz)
		return item as? T
	}

	override fun <T : UIProperty> remove(clazz: KClass<T>): T? {
		val item = values.remove(clazz) as? T
		if (item != null) {
			propertySignal.set(item)
		}
		return item
	}

}