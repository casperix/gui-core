package casper.gui

import casper.collection.ObservableMutableSet
import casper.core.Disposable
import casper.geometry.Vector2d
import casper.geometry.Vector2i
import casper.gui.component.UIComponent
import casper.gui.layout.Layout
import casper.gui.layout.LayoutTarget
import casper.signal.concrete.Future
import casper.signal.concrete.Promise
import casper.signal.concrete.StoragePromise
import kotlin.reflect.KClass

interface UINode : LayoutTarget, Disposable {
	val propertyCollection: UIPropertyCollection
	val propertySignal: Promise<UIProperty>

	val uiScene: UIScene
	val components: ObservableMutableSet<UIComponent>
	val children: ObservableMutableSet<UINode>

	var layout: Layout?
	var isHitByArea: Boolean
	var mouseEventEnabled: Boolean
	var mouseEventCaptured: Boolean

	val onScene: StoragePromise<Boolean>
	val onInvalidate: Future<UINode>
	val onTransform: Future<UINode>
	val onDispose: Future<UINode>
	val dispatcher: NodeInputDispatcher

	override fun dispose()

	fun getParent(): UINode?

	override fun setPosition(position: Vector2d): UINode
	override fun setPreferredSize(size: Vector2d): UINode
	fun getPosition(): Vector2d
	override fun getSize(): Vector2d
	override fun getPreferredSize(): Vector2d

	fun invalidate()

	fun setPosition(position: Vector2i): UINode {
		this.setPosition(position.toVector2d())
		return this
	}

	fun setPosition(x: Int, y: Int): UINode {
		this.setPosition(Vector2d(x.toDouble(), y.toDouble()))
		return this
	}

	fun setPosition(x: Double, y: Double): UINode {
		this.setPosition(Vector2d(x, y))
		return this
	}

	fun setSize(position: Vector2i): UINode {
		this.setPreferredSize(position.toVector2d())
		return this
	}

	fun setSize(x: Int, y: Int): UINode {
		this.setPreferredSize(Vector2d(x.toDouble(), y.toDouble()))
		return this
	}

	fun setSize(x: Double, y: Double): UINode {
		this.setPreferredSize(Vector2d(x, y))
		return this
	}

	fun getName(): String



	fun setClipChildren(value: Boolean)

	operator fun plusAssign(other: UINode) {
		this.children.add(other)
	}

	operator fun minusAssign(other: UINode) {
		this.children.remove(other)
	}

	fun <T : UIProperty> getProperty(clazz: KClass<T>): T? {
		val selfProperty = propertyCollection.get(clazz)
		if (selfProperty != null) return selfProperty

		val parent = getParent()
		if (parent != null) {
			val parentProperty = parent.getProperty(clazz)
			if (parentProperty != null) return parentProperty
		}
		val root = uiScene.root
		if (root != this) {
			val mainProperty = root.getProperty(clazz)
			return mainProperty
		}
		return null
	}

	fun setParent(parent: UINode): UINode {
		parent.children.add(this)
		return this
	}

}

inline fun <reified T : UIProperty> UINode.getProperty(): T? {
	return getProperty(T::class)
}

inline fun <reified T : UIProperty> UINode.setProperty(value: T):UINode {
	propertyCollection.set(value)
	return this
}


inline fun <reified T : UIComponent> UINode.getComponentOrNull(): T? {
	components.forEach { component ->
		if (component is T) {
			return component
		}
	}
	return null
}

inline fun <reified T : UIComponent> UINode.getComponent(): T {
	return getComponentOrNull<T>() ?: throw Error("Component ${T::class} not founded")
}

inline fun <reified T : UIComponent> UINode.update(builder: (T) -> Unit) {
	getComponentOrNull<T>()?.let {
		builder(it)
		invalidate()
	}
}

