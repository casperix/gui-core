package casper.gui

interface UINodeFactory {
	fun create(): UINode
}