package casper.gui

import casper.signal.concrete.Future
import casper.signal.concrete.Signal
import kotlin.reflect.KClass

interface UIPropertyCollection {
	val propertySignal:Future<UIProperty>
	fun <T : UIProperty> get(clazz: KClass<T>): T?
	fun <T : UIProperty> set(value: T)
	fun <T : UIProperty> remove(clazz: KClass<T>):T?
}