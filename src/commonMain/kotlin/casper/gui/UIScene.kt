package casper.gui

import casper.input.InputDispatcher
import casper.signal.concrete.Future


interface UIScene {
	val nodeFactory: UINodeFactory

	val root: UINode
	val onFrame: Future<Int>
	val sceneDispatcher: InputDispatcher
	val dispatcher: InputDispatcher

	fun createNode(): UINode {
		return nodeFactory.create()
	}

}

inline fun <reified T : UIProperty> UIScene.getPropertyOrDefault(def: T): T {
	return root.getProperty() ?: def
}

inline fun <reified T : UIProperty> UIScene.getProperty(): T? {
	return root.getProperty()
}

inline fun <reified T : UIProperty> UIScene.setProperty(value: T) {
	root.propertyCollection.set(value)
}