package casper.gui.component

import casper.gui.UINode
import casper.gui.UIScene
import casper.types.ColorFill
import casper.types.FillStyle
import casper.types.RED
import casper.types.setAlpha

class UIImage(node: UINode, fillStyle: FillStyle) : UIComponent(node) {

	companion object {
		fun create(uiScene: UIScene, fillStyle: FillStyle): UIImage {
			return UIImage(uiScene.createNode(), fillStyle)
		}

		fun createEmpty(uiScene: UIScene): UIImage {
			return UIImage(uiScene.createNode(), ColorFill(RED.setAlpha(1.0)))
		}

		fun createEmpty(uiNode: UINode): UIImage {
			return UIImage(uiNode, ColorFill(RED.setAlpha(1.0)))
		}
	}

	var fillStyle = fillStyle
		set(value) {
			field = value
			invalidate()
		}

	override fun toString(): String {
		return "UIImage(fillStyle=$fillStyle)"
	}

}