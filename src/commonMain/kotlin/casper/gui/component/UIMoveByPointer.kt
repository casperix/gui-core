package casper.gui.component

import casper.geometry.Vector2d
import casper.gui.UINode
import casper.signal.concrete.Signal
import casper.core.DisposableHolder
import casper.input.MouseButton
import casper.signal.util.then

class UIMoveByPointer(node: UINode, val eventReceiver: UINode, val movableTarget: UINode, val controlButton: MouseButton = MouseButton.LEFT) : UIComponent(node) {
	val onMove = Signal<Vector2d>()

	private val observer = DisposableHolder()
	private val observerAction = DisposableHolder()
	private var firstMousePosition: Vector2d? = null
	private var firstNodePosition: Vector2d? = null

	init {
		eventReceiver.mouseEventEnabled = true
		eventReceiver.dispatcher.onMouseDown.then(components) { if (it.button == controlButton) start(it.position) }
	}

	override fun dispose() {
		super.dispose()
		observer.dispose()
	}

	private fun move(lastMousePosition: Vector2d) {
		val firstMousePosition = this.firstMousePosition
		val firstNodePosition = this.firstNodePosition
		if (firstMousePosition != null && firstNodePosition != null) {
			val mouseDelta = lastMousePosition - firstMousePosition
			val nextNodePosition = firstNodePosition + mouseDelta
			onMove.set(nextNodePosition)
			movableTarget.setPosition(nextNodePosition)
		}
	}

	private fun start(position: Vector2d) {
		firstMousePosition = position
		firstNodePosition = movableTarget.getPosition()
		node.uiScene.dispatcher.onMouseMove.then(components) { move(it.position) }
		node.uiScene.dispatcher.onMouseUp.then(components) { if (it.button == controlButton) finish(it.position) }
	}

	private fun finish(position: Vector2d) {
		move(position)
		observerAction.dispose()
		firstMousePosition = null
		firstNodePosition = null
	}
}