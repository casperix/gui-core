package casper.gui.component

import casper.core.disposeAll
import casper.gui.UINode
import casper.signal.concrete.EmptySignal
import casper.signal.util.then

class UITimer(node: UINode, val delay: Long, val duration: Long, val loop: Boolean) : UIComponent(node) {
	companion object {
		fun createOnce(node: UINode, delay: Long, action: () -> Unit): UITimer {
			val timer = UITimer(node, delay, 0, false)
			timer.onTime.then { action() }
			timer.start()
			return timer
		}

		fun createRepeated(node: UINode, duration: Long, action: () -> Unit): UITimer {
			val timer = UITimer(node, duration, duration, true)
			timer.onTime.then { action() }
			timer.start()
			return timer
		}

		fun createRepeated(node: UINode, delay:Long, duration: Long, action: () -> Unit): UITimer {
			val timer = UITimer(node, delay, duration, true)
			timer.onTime.then { action() }
			timer.start()
			return timer
		}
	}

	val onTime = EmptySignal()
	private var accumulateMs = duration - delay

	init {
		if (loop && duration <= 0) throw Error("Duration must be positive for repeated timer (now : $duration)")
		if (delay < 0) throw Error("Delay must be non negative (now : $delay)")
	}

	fun start() {
		if (delay == 0L) {
			update(0)
		}
		node.uiScene.onFrame.then(components, ::update)
	}

	fun stop() {
		components.disposeAll()
	}

	private fun update(tick:Int) {
		accumulateMs += tick
		while (accumulateMs >= duration) {
			accumulateMs -= duration
			onTime.set()
			if (!loop) {
				dispose()
			}
		}
	}
}