package casper.gui.component.bar

import casper.gui.UIProperty
import casper.gui.component.button.ButtonBackgroundStyle
import casper.gui.component.scroll.ScrollStyle
import casper.types.*

class ProgressBarStyle(val width: Double, val backgroundStyle: FillStyle, val progressStyle: FillStyle) : UIProperty {
	companion object {
		val DEFAULT = ProgressBarStyle(20.0, ColorFill(BLACK.setAlpha(0.5)), ColorFill(WHITE.setAlpha(0.5)))
	}
}