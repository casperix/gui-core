package casper.gui.component.bar

import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.forPropertyOr
import casper.gui.component.scroll.ScrollLogic
import casper.signal.util.then

class UIProgressBar(val logic: ScrollLogic, var vertical: Boolean) : UIComponent(logic.node) {
	private val back = UIImage.createEmpty(uiScene)
	private val progress = UIImage.createEmpty(uiScene)

	init {
		forPropertyOr(ProgressBarStyle.DEFAULT) {
			if (vertical) {
				node.setSize(it.width, 100.0)
			} else {
				node.setSize(100.0, it.width)
			}
		}
		node += back.node
		node += progress.node
		node.onInvalidate.then(components) { updateStyle() }
		logic.onMin.then(components) { updateSize() }
		logic.onMax.then(components) { updateSize() }
		logic.onValue.then(components) { updateSize() }
		node.onTransform.then(components) { updateSize() }
	}

	private fun updateStyle() {
		forPropertyOr(ProgressBarStyle.DEFAULT) {
			back.fillStyle = it.backgroundStyle
			progress.fillStyle = it.progressStyle
		}
	}

	private fun updateSize() {
		val minValue = logic.onMin.value
		val max = (logic.onMax.value - minValue).toDouble()
		val value = (logic.onValue.value - minValue).toDouble()
		val nodeSize = node.getSize()

		if (vertical) {
			back.node.setPreferredSize(nodeSize)
			progress.node.setSize(nodeSize.x, nodeSize.y * value / max)
		} else {
			back.node.setPreferredSize(nodeSize)
			progress.node.setSize(nodeSize.x * value / max, nodeSize.y)
		}
	}
}