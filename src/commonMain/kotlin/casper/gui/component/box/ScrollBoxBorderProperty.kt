package casper.gui.component.box

import casper.gui.UIProperty
import casper.types.Scale9Texture

class ScrollBoxBorderProperty(val left: Scale9Texture, val right: Scale9Texture, val top: Scale9Texture, val bottom: Scale9Texture) : UIProperty