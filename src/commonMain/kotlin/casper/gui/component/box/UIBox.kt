package casper.gui.component.box

import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIMoveByPointer
import casper.gui.getProperty
import casper.gui.layout.Layout

class UIBox(node: UINode, val content: UINode) : UIComponent(node) {
	val mover = UIMoveByPointer(node, node, content)

	companion object {
		private fun createBase(uiScene: UIScene): UIBox {
			val node = uiScene.createNode()
			val box = UIBox(node, uiScene.createNode())
			val style:ScrollBoxBorderProperty? = node.getProperty()
			if (style != null) {
				UIBoxBorder(box, style)
			}
			return box
		}

		fun create(uiScene: UIScene): UIBox {
			return createExternal(uiScene)
		}

		fun createExternal(uiScene: UIScene): UIBox {
			val box = createBase(uiScene)
			box.node.setClipChildren(true)
			UINodeToContentLimit(box.node, box.content)
			return box
		}

		fun createInternal(uiScene: UIScene): UIBox {
			val box = createBase(uiScene)
			UIContentToNodeLimit(box.node, box.content)
			return box
		}
	}

	init {
		node += content
//		content.layout = Layout.BORDER
	}
}

