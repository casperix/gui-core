package casper.gui.component.box

import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.signal.util.then

class UIBoxBorder(val box: UIBox, val border: ScrollBoxBorderProperty) : UIComponent(box.node) {
	private val left = UIImage.create(uiScene, border.left)
	private val right = UIImage.create(uiScene, border.right)
	private val top = UIImage.create(uiScene, border.top)
	private val bottom = UIImage.create(uiScene, border.bottom)

	init {
		node.onTransform.then(components) { updateBorder() }
		box.content.onTransform.then(components) { updateBorder() }
		updateBorder()
	}

	override fun dispose() {
		super.dispose()
		left.dispose()
		right.dispose()
		top.dispose()
		bottom.dispose()
	}

	private fun updateBorder() {
		val firstCorner = box.content.getPosition()
		val lastCorner = box.content.getPosition() + box.content.getSize()

		updateImage(left, firstCorner.x < 0)
		updateImage(right, lastCorner.x > node.getSize().x)
		updateImage(top, firstCorner.y < 0)
		updateImage(bottom, lastCorner.y > node.getSize().y)
	}

	private fun updateImage(image: UIImage, visible: Boolean) {
		if (visible) {
			if (image.node.getParent() != node) {
				node += image.node
			}
			image.node.setPreferredSize(node.getSize())
		}
		if (!visible && image.node.getParent() == node) {
			node -= image.node
		}
	}
}