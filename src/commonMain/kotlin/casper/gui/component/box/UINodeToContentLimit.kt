package casper.gui.component.box

import casper.geometry.Vector2d
import casper.gui.UINode
import casper.gui.component.UIComponent
import casper.core.DisposableHolder
import casper.signal.util.then

class UINodeToContentLimit(node: UINode, val content: UINode) : UIComponent(node) {

	init {
		components.add(content)
		content.onTransform.then(components) { update() }
		node.onTransform.then(components) { update() }
	}


	private fun update() {
		var position = content.getPosition()
		position = position.upper(node.getSize() - content.getSize())
		position = position.lower(Vector2d.ZERO)
		content.setPosition(position)
	}
}