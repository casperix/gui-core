package casper.gui.component.button

import casper.gui.UINode
import casper.gui.component.UIComponent
import casper.input.MouseUp
import casper.signal.concrete.Signal
import casper.signal.concrete.StorageSignal
import casper.signal.util.then

class ButtonLogic(node: UINode, click: (() -> Unit)? = null) : UIComponent(node) {
	val onClick = Signal<Unit>()
	val onEnabled = StorageSignal(true)
	val onPressed = StorageSignal(false)
	val onFocused = StorageSignal(false)

	val isEnabled get() = onEnabled.value
	val isPressed get() = onPressed.value
	val isFocused get() = onFocused.value


	init {
		if (click != null) {
			onClick.then { click() }
		}

		node.mouseEventEnabled = true
		node.mouseEventCaptured = true
		node.dispatcher.onMouseDown.then(components) {
			if (onEnabled.value) {
				onPressed.set(true)
			}
		}

		val onMouseUp = { _: MouseUp ->
			if (onEnabled.value && onPressed.value) {
				onClick.set(Unit)
			}
			if (onPressed.value) {
				onPressed.set(false)
			}
		}
		node.dispatcher.onMouseUp.then(components, onMouseUp)
		node.uiScene.dispatcher.onMouseUp.then(components, onMouseUp)


		node.dispatcher.onMouseOver.then(components) {
			if (onEnabled.value) {
				onFocused.set(true)
			}
		}
		node.dispatcher.onMouseOut.then(components) {
			onFocused.set(false)
		}
	}

}