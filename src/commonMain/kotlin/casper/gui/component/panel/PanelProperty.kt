package casper.gui.component.panel

import casper.gui.UIProperty
import casper.types.Color4d
import casper.types.ColorFill
import casper.types.FillStyle

class PanelProperty(val fillStyle: FillStyle) : UIProperty {
	companion object {
		val DEFAULT = PanelProperty(ColorFill(Color4d(0.2, 0.2, 0.2, 0.6)))
	}
}