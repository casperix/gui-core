package casper.gui.component.scroll

import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.UIMoveByPointer
import casper.gui.component.box.UIContentToNodeLimit
import casper.gui.component.button.ButtonLogic
import casper.gui.component.forProperty
import casper.signal.concrete.StorageSignal
import casper.signal.util.then
import kotlin.math.max

class UIScroll(val logic: ScrollLogic, var vertical: Boolean = true) : UIComponent(logic.node) {
	companion object {
		fun create(uiScene: UIScene, vertical:Boolean, value: Double = 0.0, min: Double = 0.0, max: Double = 1.0, handler:((Double)->Unit)? = null): UIScroll {
			val logic = ScrollLogic.create(uiScene, value, min, max, handler)
			return UIScroll(logic, vertical)
		}
	}

	private val slider = UIImage.createEmpty(uiScene)
	private val button = ButtonLogic(node)

	val back = UIImage.createEmpty(node)
	val onThick = StorageSignal(0.0)

	init {
		forProperty<ScrollStyle> {
			onThick.set(it.preferredSliderThick)
		}

		val onMoveSlider = UIMoveByPointer(node, node, slider.node).onMove
		UIContentToNodeLimit(node, slider.node)

		node += slider.node
		logic.onMin.then(components) { setSliderByLogic() }
		logic.onMax.then(components) { setSliderByLogic() }
		logic.onValue.then(components) { setSliderByLogic() }
		onThick.then(components) { setSliderByLogic() }

		button.onEnabled.then(components) { updateStyle() }
		button.onFocused.then(components) { updateStyle() }
		button.onPressed.then(components) { updateStyle() }
		node.onInvalidate.then(components) { updateStyle() }

		node.onTransform.then(components) { setSliderByLogic() }
		onMoveSlider.then(components) { setLogicBySlider() }
	}

	private fun updateStyle() {
		forProperty<ScrollStyle> {
			back.fillStyle = it.backgroundStyle.getBackFill(button)
			slider.fillStyle = it.sliderStyle.getBackFill(button)
		}
	}

	private fun setLogicBySlider() {
		val minValue = logic.onMin.value
		val maxValue = logic.onMax.value
		val nodeSize = node.getSize()
		val thick = getSliderThickInPixels()

		val value = if (vertical) {
			(slider.node.getPosition().y * (maxValue - minValue) / max(1.0, nodeSize.y - thick))
		} else {
			(slider.node.getPosition().x * (maxValue - minValue) / max(1.0, nodeSize.x - thick))
		}

		logic.setValue((value + minValue))
	}

	private fun setSliderByLogic() {
		val minValue = logic.onMin.value
		val max = logic.onMax.value - minValue
		val value = logic.onValue.value - minValue
		val nodeSize = node.getSize()
		val thick = getSliderThickInPixels()

		if (vertical) {
			slider.node.setPosition(0.0, (nodeSize.y - thick) * value / max)
			slider.node.setSize(nodeSize.x, thick)
		} else {
			slider.node.setPosition((nodeSize.x - thick) * value / max, 0.0)
			slider.node.setSize(thick, nodeSize.y)
		}
	}

	private fun getSliderThickInPixels(): Double {
		var minSliderThick = 10.0
		forProperty<ScrollStyle> { minSliderThick = it.minSliderThick }
		return max(minSliderThick, onThick.value)
	}
}