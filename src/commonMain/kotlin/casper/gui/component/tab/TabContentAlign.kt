package casper.gui.component.tab

import casper.gui.layout.Align

class TabContentAlign(val absolute: Boolean = false, val align: Align = Align.CENTER, val alignIfOverhead:Align = Align.MIN, val clamp:Boolean = true)