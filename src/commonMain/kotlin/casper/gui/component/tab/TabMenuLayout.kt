package casper.gui.component.tab

import casper.geometry.Vector2d
import casper.gui.UINode
import casper.gui.layout.*
import casper.math.clamp
import casper.signal.concrete.Signal
class TabMenuLayout(val layoutInfo: TabMenuLayoutInfo, val style: TabMenuStyle, val toggleTarget: UINode, val contentTarget: UINode, val onTabSelected: Signal<UITab>) : Layout {
	private var selectedTab: UITab? = null
	private val toggleTargetOrientation = if (layoutInfo.toggleSide == Direction.LEFT || layoutInfo.toggleSide == Direction.RIGHT) {
		Orientation.VERTICAL
	} else {
		Orientation.HORIZONTAL
	}

	init {
		toggleTarget.layout = OrientationLayout(toggleTargetOrientation, Align.CENTER, style.tabBorder, style.tabGap)
		contentTarget.layout = BorderLayout(style.contentGap)
		onTabSelected.then {
			selectedTab = it
		}
	}

	private fun getContentStart(): Double {
		val info = layoutInfo.contentAlign

		val targetSize = if (toggleTargetOrientation == Orientation.HORIZONTAL) {
			toggleTarget.getSize().x
		} else {
			toggleTarget.getSize().y
		}

		val contentSize = if (toggleTargetOrientation == Orientation.HORIZONTAL) {
			contentTarget.getSize().x
		} else {
			contentTarget.getSize().y
		}

		if (info.absolute) {
			return info.align.getPosition(targetSize, contentSize)
		}


		val contentCenterMin =
				if (toggleTargetOrientation == Orientation.HORIZONTAL) {
					selectedTab?.let { it.toggle.getPosition().x + it.toggle.getSize().x * 0.0 } ?: 0.0
				} else {
					selectedTab?.let { it.toggle.getPosition().y + it.toggle.getSize().y * 0.0 } ?: 0.0
				}

		val contentCenterMax =
				if (toggleTargetOrientation == Orientation.HORIZONTAL) {
					selectedTab?.let { it.toggle.getPosition().x + it.toggle.getSize().x * 1.0 } ?: 0.0
				} else {
					selectedTab?.let { it.toggle.getPosition().y + it.toggle.getSize().y * 1.0 } ?: 0.0
				}

		if (targetSize < contentSize) {
			return info.alignIfOverhead.getPosition(targetSize, contentSize)
		} else {
			val contentPosition = contentCenterMin + info.align.getPosition(contentCenterMax - contentCenterMin, contentSize)
			if (info.clamp) {
				return contentPosition.clamp(0.0, targetSize - contentSize)
			} else {
				return contentPosition
			}
		}
	}

	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>): Vector2d {
		return (contentTarget.getPosition() + contentTarget.getSize()).upper(toggleTarget.getPosition() + toggleTarget.getSize())
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>) {
		val contentAlignOffset = getContentStart()

		when (layoutInfo.toggleSide) {
			Direction.LEFT -> {
				toggleTarget.setPosition(.0, .0)
				contentTarget.setPosition(toggleTarget.getSize().x, contentAlignOffset)
			}
			Direction.RIGHT -> {
				contentTarget.setPosition(.0, contentAlignOffset)
				toggleTarget.setPosition(contentTarget.getSize().x, .0)
			}
			Direction.TOP -> {
				toggleTarget.setPosition(.0, .0)
				contentTarget.setPosition(contentAlignOffset, toggleTarget.getSize().y)
			}
			Direction.BOTTOM -> {
				contentTarget.setPosition(contentAlignOffset, .0)
				toggleTarget.setPosition(.0, contentTarget.getSize().y)
			}
		}
	}
}