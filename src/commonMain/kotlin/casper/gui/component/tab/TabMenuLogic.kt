package casper.gui.component.tab

import casper.collection.ObservableMutableSet
import casper.core.Disposable
import casper.gui.util.BooleanGroupController
import casper.gui.util.BooleanGroupRule
import casper.signal.concrete.StoragePromise

class TabMenuLogic(rule: BooleanGroupRule) : Disposable {
	val values = ObservableMutableSet<StoragePromise<Boolean>>()
	val groupController = BooleanGroupController(values, rule)

	override fun dispose() {
		groupController.dispose()
		values.clear()
	}
}