package casper.gui.component.tab

import casper.geometry.Vector2d
import casper.gui.UIProperty
import casper.gui.layout.Direction
import casper.gui.util.BooleanGroupRule
import casper.types.FillStyle

class TabMenuLayoutInfo(val toggleSide: Direction, val contentAlign: TabContentAlign)

class TabMenuStyle(
		val titleBack: FillStyle? = null,
		val contentBack: FillStyle? = null,
		val tabBorder: Vector2d = Vector2d(5.0),
		val tabGap: Vector2d = Vector2d(5.0),
		val contentGap: Vector2d = Vector2d(5.0)
) : UIProperty {
	companion object {
		val DEFAULT = TabMenuStyle()
	}
}