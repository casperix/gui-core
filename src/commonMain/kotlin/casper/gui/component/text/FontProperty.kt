package casper.gui.component.text

import casper.gui.UIProperty

class FontProperty (val name:String, val size:Int, val italic:Boolean, val bold:Boolean, val underline:Boolean) : UIProperty{
	companion object {
		val DEFAULT = FontProperty("Arial",  16, false, false, false)
	}
}