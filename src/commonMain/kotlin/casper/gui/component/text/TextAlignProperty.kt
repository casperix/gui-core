package casper.gui.component.text

import casper.gui.UIProperty
import casper.gui.layout.Align

class TextAlignProperty(val verticalAlign: Align, val horizontalAlign: Align) : UIProperty {
	companion object {
		val DEFAULT = TextAlignProperty(Align.CENTER, Align.CENTER)
	}
}