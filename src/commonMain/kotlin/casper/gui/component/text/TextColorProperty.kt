package casper.gui.component.text

import casper.gui.UIProperty
import casper.types.BLACK
import casper.types.Color3d
import casper.types.Color4d
import casper.types.setAlpha

class TextColorProperty(val color: Color4d) : UIProperty {
	companion object {
		val DEFAULT = TextColorProperty(BLACK.setAlpha(1.0))
	}
}