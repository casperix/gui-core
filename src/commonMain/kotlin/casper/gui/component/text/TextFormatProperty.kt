package casper.gui.component.text

import casper.gui.UIProperty
import casper.gui.layout.Align

class TextFormatProperty(val wrapping:Boolean, val lineSpacing:Double, val resizeToFit: Boolean) : UIProperty {
	companion object {
		val DEFAULT = TextFormatProperty(false, 0.0, false)
	}
}