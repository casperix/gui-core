package casper.gui.component.text

import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.signal.concrete.StorageSignal

class TextInputLogic(node: UINode, startText: String) : UIComponent(node) {
	val onText = StorageSignal(startText)

	companion object {
		fun create(uiScene: UIScene, startText: String, onTextChanged: (String) -> Unit = {}): TextInputLogic {
			val input = TextInputLogic(uiScene.createNode(), startText)
			input.onText.then(onTextChanged)
			return input
		}
	}
}