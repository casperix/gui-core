package casper.gui.component.text

import casper.gui.UIProperty
import casper.types.Color4d
import casper.types.ColorFill
import casper.types.FillStyle

class TextInputProperty(val selectionColor: Color4d, val normalFill: FillStyle, val focusedFill: FillStyle) : UIProperty {
	companion object {
		val DEFAULT = TextInputProperty(Color4d(0.0, 0.0, 0.0, 0.6), ColorFill(Color4d(0.3, 0.3, 0.3, 0.8)), ColorFill(Color4d(0.6, 0.6, 0.6, 0.8)))
	}
}