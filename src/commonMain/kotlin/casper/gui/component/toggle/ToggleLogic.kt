package casper.gui.component.toggle

import casper.gui.component.UIComponent
import casper.gui.component.button.ButtonLogic
import casper.signal.concrete.StoragePromise
import casper.signal.util.switch
import casper.signal.util.then

class ToggleLogic(val button: ButtonLogic, val switch: StoragePromise<Boolean>) : UIComponent(button.node) {
	init {
		button.onClick.then {
			switch.switch()
		}
	}

	fun addListener(listener:(Boolean)->Unit) {
		switch.then(components, listener)
	}

}