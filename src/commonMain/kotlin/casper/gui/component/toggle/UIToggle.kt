package casper.gui.component.toggle

import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.button.ButtonLogic
import casper.gui.component.button.UIButton
import casper.gui.component.forProperty
import casper.gui.component.text.UIText
import casper.gui.getProperty
import casper.signal.concrete.StoragePromise
import casper.signal.concrete.StorageSignal
import casper.signal.util.then
import casper.types.ScaleTexture
import casper.types.Texture

typealias UIToggleWithImage = UIToggle<UIImage>
typealias UIToggleWithLabel = UIToggle<UIText>

fun UIToggleWithLabel.setText(value: String) {
	content.text = value
}

fun UIToggleWithImage.setImage(texture: Texture) {
	val p: ToggleStyle? = content.node.getProperty()
	content.fillStyle = casper.types.ScaleTexture(texture, p?.on?.contentGap ?: 0.0)
}

class UIToggle<Content : UIComponent>(val logic: ToggleLogic, val content: Content) : UIComponent(logic.node) {
	companion object {
		fun <Content : UIComponent> create(uiScene: UIScene, content: Content, switch: StoragePromise<Boolean>? = null): UIToggle<Content> {
			val node = uiScene.createNode()
			val logic = ButtonLogic(node)
			val promise = if (switch != null) switch else StorageSignal(false)
			val toggle = ToggleLogic(logic, promise)
			return UIToggle(toggle, content)
		}

		fun <Content : UIComponent> create(uiScene: UIScene, content: Content, on: Boolean, switch: (Boolean) -> Unit): UIToggle<Content> {
			val toggle = create(uiScene, content)
			toggle.logic.switch.set(on)
			toggle.logic.addListener(switch)
			return toggle
		}

		fun createWithText(uiScene: UIScene, text: String, on: Boolean, switch: (Boolean) -> Unit): UIToggle<UIText> {
			return create(uiScene, UIText.create(uiScene, text), on, switch)
		}

		fun createWithText(uiScene: UIScene, text: String, switch: StoragePromise<Boolean>? = null): UIToggle<UIText> {
			return create(uiScene, UIText.create(uiScene, text), switch)
		}

		fun createWithImage(uiScene: UIScene, texture: Texture, on: Boolean, switch: (Boolean) -> Unit): UIToggle<UIImage> {
			return create(uiScene, UIImage.create(uiScene, ScaleTexture(texture, 0.0)), on, switch)
		}

		fun createWithImage(uiScene: UIScene, texture: Texture, switch: StoragePromise<Boolean>? = null): UIToggle<UIImage> {
			return create(uiScene, UIImage.create(uiScene, ScaleTexture(texture, 0.0)), switch)
		}
	}

	private val button = UIButton(logic.button, content)

	init {
		refresh()
		logic.switch.then(components) { refresh() }
		node.propertySignal.then(components) { refresh() }
	}

	private fun refresh() {
		val on = logic.switch.value
		forProperty<ToggleStyle> {
			val buttonProperty = if (on) it.on else it.off
			button.node.propertyCollection.set(buttonProperty)
			button.invalidate()
		}
	}
}