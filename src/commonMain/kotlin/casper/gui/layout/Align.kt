package casper.gui.layout

class Align(val factor: Double) {
	init {
		if (factor < 0.0 || factor > 1.0) throw Error("Must be in interval [0, 1] (now: $factor)")
	}

	companion object {
		val MIN = Align(0.0)
		val CENTER = Align(0.5)
		val MAX = Align(1.0)
	}

	fun getPosition(parentSize: Double, childSize: Double): Double {
	//	if (parentSize < childSize) return 0.0
		return (parentSize - childSize) * factor
	}
}