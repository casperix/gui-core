package casper.gui.layout

import casper.geometry.Vector2d

class BorderLayout(val gap: Vector2d = Vector2d(5.0)) : Layout {

	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>): Vector2d {
		var dimension = Vector2d.ZERO

		for (child in children) {
			dimension = dimension.upper(gap + child.getSize() + gap)
		}
		return dimension
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>) {
		for (child in children) {
			child.setPosition(gap)
		}
	}
}