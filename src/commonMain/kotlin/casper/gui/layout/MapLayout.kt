package casper.gui.layout

import casper.geometry.Vector2d
import casper.gui.UINode


/**
 *	For child by index N use layouts by index N. If if layout not exist, then nothing
 */
class MapLayout(val layouts: Map<UINode, Layout>) : Layout {
	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>): Vector2d {
		var result = Vector2d.ZERO
		for (child in children) {
			val layout = layouts.get(child)
			if (layout != null) {
				val subArea = layout.selfLayout(area, listOf(child))
				result = result.upper(subArea)
			}
		}
		return result
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>) {
		for (child in children) {
			val layout = layouts.get(child)
			if (layout != null) {
				layout.childrenLayout(area, listOf(child))
			}
		}
	}
}