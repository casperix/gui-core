package casper.gui.layout

import casper.geometry.Vector2d


/**
 *	For child by index N use layouts by index N. If if layout not exist, then nothing
 */
class MultiLayout(val layouts: List<Layout>) : Layout {

	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>): Vector2d {
		var childIndex = 0
		var result = Vector2d.ZERO
		for (child in children) {
			if (layouts.size <= childIndex) break
			val next = layouts[childIndex].selfLayout(area, listOf(child))
			result = result.upper(next)
			++childIndex
		}
		return result
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>) {
		var childIndex = 0
		for (child in children) {
			if (layouts.size <= childIndex) break
			val next = layouts[childIndex].childrenLayout(area, listOf(child))
			++childIndex
		}
	}

	constructor(vararg layouts: Layout) : this(layouts.toList())
}