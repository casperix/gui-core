package casper.gui.layout

enum class Orientation {
	VERTICAL,
	HORIZONTAL
}