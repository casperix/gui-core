package casper.gui.layout

import casper.geometry.Vector2d

class OrientationLayout(val orientation: Orientation, val align: Align = Align.CENTER, val border: Vector2d = Vector2d(5.0), val gap: Vector2d = Vector2d(5.0)) : Layout {

	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>): Vector2d {
		var maxSize = Vector2d.ZERO
		for (child in children) {
			maxSize = maxSize.upper(child.getSize())
		}

		var position = border
		var dimension = Vector2d.ZERO


		for (child in children) {
			val childSize = child.getSize()
			val childOffset = if (orientation == Orientation.VERTICAL) {
				Vector2d(align.getPosition(maxSize.x, childSize.x), 0.0)
			} else {
				Vector2d(0.0, align.getPosition(maxSize.y, childSize.y))
			}

			dimension = dimension.upper(position + childOffset + childSize)

			position += if (orientation == Orientation.VERTICAL) {
				Vector2d(0.0, child.getSize().y + gap.y)
			} else {
				Vector2d(child.getSize().x + gap.x, 0.0)
			}
		}
		return dimension + border
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>) {
		var maxSize = Vector2d.ZERO
		for (child in children) {
			maxSize = maxSize.upper(child.getSize())
		}

		var position = border

		for (child in children) {
			val childSize = child.getSize()
			val childOffset = if (orientation == Orientation.VERTICAL) {
				Vector2d(align.getPosition(maxSize.x, childSize.x), 0.0)
			} else {
				Vector2d(0.0, align.getPosition(maxSize.y, childSize.y))
			}

			child.setPosition(position + childOffset)

			position += if (orientation == Orientation.VERTICAL) {
				Vector2d(0.0, child.getSize().y + gap.y)
			} else {
				Vector2d(child.getSize().x + gap.x, 0.0)
			}
		}
	}
}