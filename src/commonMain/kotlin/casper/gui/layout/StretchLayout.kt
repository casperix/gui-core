package casper.gui.layout

import casper.geometry.Vector2d
import casper.types.Padding

class StretchLayout(val padding: Padding) : Layout {

	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>):Vector2d {
		return area
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>) {
		val upperCorner = Vector2d(padding.left.toDouble(), padding.top.toDouble())
		val lowerCorner = Vector2d(padding.right.toDouble(), padding.bottom.toDouble())
		for (child in children) {
			child.setPosition(upperCorner)
			child.setPreferredSize(area - upperCorner - lowerCorner)
		}
	}
}