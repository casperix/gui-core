package casper.gui.tree

import casper.gui.UINode

class TreeNode(val content: (TreeNode) -> UINode, val children: MutableList<TreeNode>, val tag: Any? = null) {
	constructor(builder: (TreeNode) -> UINode) : this(builder, mutableListOf())

	fun firstOrNull(checker: (TreeNode) -> Boolean): TreeNode? {
		if (checker(this)) return this
		children.forEach {
			val result = it.firstOrNull(checker)
			if (result != null) return result
		}
		return null
	}
}