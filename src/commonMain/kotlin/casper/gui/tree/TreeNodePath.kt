package casper.gui.tree

import casper.signal.concrete.Signal

class TreeNodePath(val root: TreeNode) {
	var parts = listOf<TreeNode>()
		set(value) {
			if (field != value) {
				field = value
				onChanged.set(true)
			}
		}


	val onChanged = Signal<Boolean>()

	init {
		parts = listOf(root)
	}

	fun select(next: TreeNode): Boolean {
		val nextPath = searchPath(root, next)
		if (nextPath != null) {
			parts = nextPath
			return true
		}
		return false
	}

	fun searchPath(parent: TreeNode, next: TreeNode): List<TreeNode>? {
		if (parent.children.contains(next)) {
			return listOf(parent, next)
		}
		for (child in parent.children) {
			val list = searchPath(child, next)
			if (list != null) {
				val result = mutableListOf(parent)
				result.addAll(list)
				return result
			}
		}
		return null
	}

	fun switch(node: TreeNode) {
		val selectedIndex = parts.indexOf(node)
		if (selectedIndex >= 0) {
			parts = parts.slice(0 until selectedIndex)
		} else {
			select(node)
		}
	}

	fun contains(node: TreeNode): Boolean {
		return parts.contains(node)
	}
}