package casper.gui.tree

import casper.geometry.Vector2d
import casper.gui.component.button.ButtonBackgroundStyle
import casper.types.FillStyle

enum class TreeDirection {
	LEFT,
	RIGHT,
	BOTTOM,
	TOP
}
class TreeStyle (val connectorSize:Vector2d, val panelStyle:FillStyle, val buttonStyle: ButtonBackgroundStyle, val imageButtonSize: Vector2d, val labelButtonSize: Vector2d)