package casper.gui.tree

import casper.gui.UIScene
import casper.gui.UINode
import casper.gui.component.UIComponent

class UITree(node:UINode, val direction: TreeDirection, val style: TreeStyle) : UIComponent(node) {
	companion object {
		fun create(uiFactory: UIScene, direction: TreeDirection, style: TreeStyle): UINode {
			val tree = UITree(uiFactory.createNode(), direction, style)
			UITreeRender(tree, uiFactory.onFrame, uiFactory)
			return tree.node
		}
	}

	val root = TreeNode { node }
	val path = TreeNodePath(root)
//
//	fun createSwitcher(label: String, operation: () -> Unit, children: MutableList<TreeNode>, tag: Any? = null): TreeNode {
//		return TreeNode({ node ->
//			createImageButton(label) {
//				path.switch(node)
//				operation.invoke()
//			}.setSize(style.labelButtonSize)
//		}, children, tag)
//	}
//
//	fun createSwitcher(image: Texture, operation: () -> Unit, children: MutableList<TreeNode>): TreeNode {
//		return TreeNode({ node ->
//			UIButtonWithImage.create(uiFactory, style.buttonStyle, image) {
//				path.switch(node)
//				operation.invoke()
//			}.setSize(style.imageButtonSize)
//		}, children)
//	}
//
//	fun createElement(label: String, operation: () -> Unit): TreeNode {
//		return TreeNode {
//			createImageButton(label, operation).setSize(style.labelButtonSize)
//		}
//	}
//
//	fun createElement(image: Texture, operation: () -> Unit): TreeNode {
//		return TreeNode {
//			UIButtonWithImage.create(uiFactory, style.buttonStyle, image, operation).setSize(style.imageButtonSize)
//		}
//	}
//
//	private fun createImageButton(label: String, operation: () -> Unit): UINode {
//		return UIButtonWithLabel.create(uiFactory, style.buttonStyle, label, operation)
//	}
}

