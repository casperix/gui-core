package casper.gui.tree

import casper.core.Disposable
import casper.geometry.Vector2d
import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIImage
import casper.gui.layout.Layout
import casper.signal.concrete.Future
import casper.signal.concrete.Slot

class UITreeRender(tree: UITree, onFrame: Future<Int>, uiFactory: UIScene) {
	companion object {
		val MAX_SIZE = 1800
	}

	val root = uiFactory.createNode()


	init {
		root.setSize(MAX_SIZE, MAX_SIZE)

		val offset = when (tree.direction) {
			TreeDirection.RIGHT -> -Vector2d.X * root.getSize() / 2.0
			TreeDirection.LEFT -> Vector2d.X * root.getSize() / 2.0
			TreeDirection.BOTTOM -> -Vector2d.Y * root.getSize() / 2.0
			TreeDirection.TOP -> Vector2d.Y * root.getSize() / 2.0
		}

		val uiTree = UITreeListRender(tree.style, tree.direction, tree.root.children, tree.path, onFrame, uiFactory)
		uiTree.root.setPosition(offset)
		root.setClipChildren(false)
		root += uiTree.root
	}
}

class UITreeListRender(val style: TreeStyle, val direction: TreeDirection, val nodes: MutableList<TreeNode>, val path: TreeNodePath, val onFrame: Future<Int>, val uiFactory: UIScene) : Disposable {
	companion object {
		val MAX_SIZE = 1800
	}

	/*
		render children
	 */
	val root = uiFactory.createNode()
	val uiContent = UIImage.create(uiFactory, style.panelStyle)
	val uiHandle = UIImage.create(uiFactory, style.panelStyle)
	var uiNext: UITreeListRender? = null
	var observer: Slot?

	val contentMap = mutableMapOf<TreeNode, UINode>()
	var lastNodes = emptyList<TreeNode>()
	var lastSelected: TreeNode? = null

	init {
		root.setClipChildren(false)
		root.setSize(MAX_SIZE, MAX_SIZE)
		root += uiHandle.node
		root += uiContent.node
		uiContent.node.layout = if (direction == TreeDirection.LEFT || direction == TreeDirection.RIGHT) Layout.VERTICAL else Layout.HORIZONTAL

		observer = onFrame.then {
			invalidate()
		}
	}

	private fun isDisposed(): Boolean {
		return observer == null
	}

	override fun dispose() {
		uiContent.dispose()
		uiHandle.dispose()

		uiNext?.dispose()
		uiNext = null
		observer?.dispose()
		observer = null
		root.dispose()
	}

	private fun updateContent() {
		if (lastNodes == nodes) return

		lastNodes.forEach {
			dropNode(it)
		}
		lastNodes = nodes.subList(0, nodes.size)
		lastNodes.forEach {
			createNode(it)
		}
	}

	private fun createNode(value: TreeNode) {
		val node = value.content(value)
		contentMap.set(value, node)
		uiContent.node += node
	}

	private fun dropNode(value: TreeNode) {
		contentMap.remove(value)?.dispose()
	}

	private fun invalidate() {
		updateContent()
		updateChildren()
		updateLayout()
	}

	private fun updateChildren() {
		val selectedTreeNode = nodes.filter { path.parts.contains(it) }.firstOrNull()
		if (lastSelected != selectedTreeNode) {
			lastSelected = selectedTreeNode

			if (selectedTreeNode != null) {
				val uiSubTreeNext = UITreeListRender(style, direction, selectedTreeNode.children, path, onFrame, uiFactory)
				uiNext?.dispose()
				uiNext = uiSubTreeNext

				root -= (uiContent.node)
				root += (uiSubTreeNext.root)
				root += (uiContent.node)

			} else {
				uiNext?.dispose()
				uiNext = null
			}
		}
	}

	private fun updateLayout() {
		val normal = when (direction) {
			TreeDirection.RIGHT -> Vector2d.X
			TreeDirection.LEFT -> -Vector2d.X
			TreeDirection.BOTTOM -> Vector2d.Y
			TreeDirection.TOP -> -Vector2d.Y
		}
		val direction = when (direction) {
			TreeDirection.RIGHT -> Vector2d.Y
			TreeDirection.LEFT -> -Vector2d.Y
			TreeDirection.BOTTOM -> Vector2d.X
			TreeDirection.TOP -> -Vector2d.X
		}
		val center = root.getSize() / 2.0
		uiContent.node.setPosition(center - direction * uiContent.node.getSize() / 2.0 + normal * style.connectorSize)
		val connectorSize = normal * style.connectorSize * 2.0 + direction * style.connectorSize * 8.0
		uiHandle.node.setPosition(center - connectorSize / 2.0)
		uiHandle.node.setPreferredSize(connectorSize)

		val uiContentNode = contentMap.get(lastSelected)
		if (uiContentNode != null) {
			val uiContentNodeCenter = uiContent.node.getPosition() + uiContentNode.getPosition() + uiContentNode.getSize() / 2.0

			val uiSubTree = uiNext
			if (uiSubTree != null) {
				val uiSubTreeRoot = uiSubTree.root
				uiSubTreeRoot.setPosition(uiContentNodeCenter - root.getSize() / 2.0 + normal * (style.connectorSize + uiContent.node.getSize() / 2.0))
			}
		}
	}
}