package casper.gui.util

import casper.collection.ObservableCollection
import casper.core.DisposableHolder
import casper.core.disposeAll
import casper.signal.concrete.StoragePromise
import casper.signal.util.then

class BooleanGroupController(val promises: ObservableCollection<StoragePromise<Boolean>>, val rule: BooleanGroupRule) : DisposableHolder() {
	init {
		restartObservers()
	}

	private fun restartObservers() {
		components.disposeAll()

		promises.then(components, { restartObservers() }, { restartObservers() })
		promises.forEach { value ->
			value.then(components) {
				update(value, it)
			}
		}
	}

	private fun update(first: StoragePromise<Boolean>, on: Boolean) {
		if (on && (rule == BooleanGroupRule.ONLY_ONE_TRUE || rule == BooleanGroupRule.MAX_ONE_TRUE)) {
			promises.forEach { value ->
				if (value != first) {
					value.set(false)
				}
			}
		}

		if (!on && (rule == BooleanGroupRule.ONLY_ONE_TRUE)) {
			val active = promises.firstOrNull { it.value }
			if (active == null) {
				first.set(true)
			}
		}
	}
}