package casper.gui.util

enum class BooleanGroupRule {
	MAX_ONE_TRUE,
	ONLY_ONE_TRUE
}