package casper.gui.util

import casper.gui.UINode
import casper.gui.component.UIComponent
import casper.signal.concrete.StorageFuture
import casper.signal.util.then

class UITabController(node: UINode, val onVisible: StorageFuture<Boolean>, val parent: UINode, val content: UINode) : UIComponent(node) {

	init {
		onVisible.then(components) {
			update()
		}
		update()
	}

	private fun update() {
		if (onVisible.value) {
			parent += content
		} else {
			parent -= content
		}
	}

	override fun dispose() {
		parent -= content
		super.dispose()
	}
}