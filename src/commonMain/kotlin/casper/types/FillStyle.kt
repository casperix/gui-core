package casper.types

interface FillStyle

class ColorFill(val color: Color4d) : FillStyle {

}

class ScaleTexture(val texture: Texture, val border: Double) : FillStyle {
	override fun toString(): String {
		return "ScaleTexture(texture=$texture, border=$border)"
	}
}

/**
 * padding -- отсуп от краев текстуры до централной части
 */
class Scale9Texture(val texture: Texture, val border: Double, val scale9: Padding) : FillStyle {
	override fun toString(): String {
		return "Scale9Texture(texture=$texture, border=$border, scale9=$scale9)"
	}
}
